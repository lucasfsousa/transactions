package br.com.lucasfelix.transactions.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.lucasfelix.transactions.domain.TransactionInput;
import br.com.lucasfelix.transactions.domain.TransactionProcessed;

public class TransactionServiceTest {
	@InjectMocks
	private TransactionService service;

	@Mock
	private InstantValidator validator;

	@Mock
	private TransactionCollector collector;

	public TransactionServiceTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldSaveWhenValid() {
		when(validator.isValid(Mockito.any(Instant.class), Mockito.any(Instant.class))).thenReturn(true);

		assertTrue(service.save(getInput()));
		verify(collector).add(Mockito.any(TransactionProcessed.class));
	}

	@Test
	public void shouldNotSaveWhenInValid() {
		when(validator.isValid(Mockito.any(Instant.class), Mockito.any(Instant.class))).thenReturn(false);

		assertFalse(service.save(getInput()));
		verify(collector, never()).add(Mockito.any(TransactionProcessed.class));
	}

	private TransactionInput getInput() {
		TransactionInput input = new TransactionInput();
		input.setAmount(123.45);
		input.setTimestamp(123456L);
		return input;
	}
}
