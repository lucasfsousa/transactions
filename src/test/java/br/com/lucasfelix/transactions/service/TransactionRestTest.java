package br.com.lucasfelix.transactions.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import br.com.lucasfelix.transactions.domain.TransactionInput;
import br.com.lucasfelix.transactions.rest.TransactionsRest;

public class TransactionRestTest {
	@InjectMocks
	private TransactionsRest rest;

	@Mock
	private TransactionService service;

	public TransactionRestTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldReturn201WhenSavedSuccessfully() {
		when(service.save(Mockito.any(TransactionInput.class))).thenReturn(true);

		TransactionInput input = new TransactionInput();
		ResponseEntity<String> response = rest.post(input);

		assertEquals(201, response.getStatusCode().value());
		verify(service).save(input);
		verifyNoMoreInteractions(service);
	}

	@Test
	public void shouldReturn204WhenNotSavedSuccessfully() {
		when(service.save(Mockito.any(TransactionInput.class))).thenReturn(false);

		TransactionInput input = new TransactionInput();
		ResponseEntity<String> response = rest.post(input);

		assertEquals(204, response.getStatusCode().value());
		verify(service).save(input);
		verifyNoMoreInteractions(service);
	}
}
