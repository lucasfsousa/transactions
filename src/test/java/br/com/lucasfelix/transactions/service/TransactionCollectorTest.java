package br.com.lucasfelix.transactions.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import br.com.lucasfelix.transactions.domain.TransactionOutput;
import br.com.lucasfelix.transactions.domain.TransactionProcessed;

public class TransactionCollectorTest {
	@InjectMocks
	@Spy
	private TransactionCollector collector;

	@Mock
	private InstantValidator validator;

	public TransactionCollectorTest() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldRemoveInvalidAndCalculateCorrectly() throws InterruptedException {
		// control transactions if they are valid
		Map<TransactionProcessed, Boolean> transactions = new HashMap<>();
		transactions.put(createTransaction(1.0), false);
		transactions.put(createTransaction(1.23), true);
		transactions.put(createTransaction(4.56), false);
		transactions.put(createTransaction(7.89), true);
		transactions.put(createTransaction(9.12), false);
		transactions.put(createTransaction(10.11), true);

		transactions.entrySet().stream().forEach(e -> {
			// teach mock
			doReturn(e.getValue().booleanValue()).when(collector).isValid(Mockito.eq(e.getKey()),
					Mockito.any(Instant.class));

			// add transactions
			collector.add(e.getKey());
		});

		collector.update();
		TransactionOutput statistics = collector.get();
		assertNotNull(statistics);
		assertEquals(3, statistics.getCount().longValue());
		assertEquals(1.23, statistics.getMin().doubleValue(), 0.01);
		assertEquals(10.11, statistics.getMax().doubleValue(), 0.01);
		assertEquals(19.23, statistics.getSum().doubleValue(), 0.01);
		assertEquals(6.41, statistics.getAvg().doubleValue(), 0.01);
	}

	public TransactionProcessed createTransaction(Double amount) {
		return new TransactionProcessed(amount, Instant.now());
	}
}
