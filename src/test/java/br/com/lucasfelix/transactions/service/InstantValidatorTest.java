package br.com.lucasfelix.transactions.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.junit.Test;

public class InstantValidatorTest {
	private InstantValidator validator = new InstantValidator(60, ChronoUnit.SECONDS);

	@Test
	public void shouldReturnValidProperly() throws InterruptedException {
		Instant instant = Instant.now();

		assertTrue("same instant should be valid", validator.isValid(instant, instant));

		assertTrue("1 seconds before should de valid", validator.isValid(instant.minusSeconds(1), instant));
		assertTrue("30 seconds before should de valid", validator.isValid(instant.minusSeconds(30), instant));
		assertTrue("60 seconds before should de valid", validator.isValid(instant.minusSeconds(60), instant));

		assertFalse("61 seconds before should de invalid", validator.isValid(instant.minusSeconds(61), instant));
		assertFalse("120 seconds before should de invalid", validator.isValid(instant.minusSeconds(120), instant));

		// future should be valid too
		assertTrue("1 seconds after should de valid", validator.isValid(instant.plusSeconds(1), instant));
		assertTrue("30 seconds after should de valid", validator.isValid(instant.plusSeconds(30), instant));
		assertTrue("60 seconds after should de valid", validator.isValid(instant.plusSeconds(60), instant));
	}
}
