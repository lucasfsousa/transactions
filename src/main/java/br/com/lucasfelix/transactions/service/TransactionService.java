package br.com.lucasfelix.transactions.service;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lucasfelix.transactions.domain.TransactionInput;
import br.com.lucasfelix.transactions.domain.TransactionOutput;
import br.com.lucasfelix.transactions.domain.TransactionProcessed;

@Service
public class TransactionService {
	private InstantValidator validator;
	private TransactionCollector collector;

	@Autowired
	public TransactionService(InstantValidator validator, TransactionCollector collector) {
		this.validator = validator;
		this.collector = collector;
	}

	public boolean save(TransactionInput input) {
		Instant instant = Instant.ofEpochMilli(input.getTimestamp());

		if (validator.isValid(instant, Instant.now())) {
			TransactionProcessed transaction = new TransactionProcessed();
			transaction.setAmount(input.getAmount());
			transaction.setInstant(instant);
			collector.add(transaction);
			return true;
		}
		return false;
	}

	public TransactionOutput getStatistics() {
		return collector.get();
	}
}
