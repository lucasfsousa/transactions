package br.com.lucasfelix.transactions.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import br.com.lucasfelix.transactions.domain.TransactionOutput;
import br.com.lucasfelix.transactions.domain.TransactionProcessed;

public class TransactionCollector {
	private Logger logger = LoggerFactory.getLogger(TransactionCollector.class);
	private static final long UPDATE_FIXED_DELAY = 1000;

	private InstantValidator validator;

	private List<TransactionProcessed> transactions;
	private TransactionOutput statistics;

	public TransactionCollector(InstantValidator validator) {
		this.validator = validator;
		statistics = new TransactionOutput();
		transactions = Collections.synchronizedList(new ArrayList<>());
	}

	public TransactionOutput get() {
		return statistics;
	}

	public void add(TransactionProcessed transaction) {
		logger.debug("Transaction added, {}", transaction);
		transactions.add(transaction);
	}

	@Scheduled(fixedDelay = UPDATE_FIXED_DELAY)
	public synchronized void update() {
		logger.debug("Updating statistics");
		Instant now = Instant.now();

		List<TransactionProcessed> transactionsToDelete = new ArrayList<>();
		long count = 0;
		double min = Double.MAX_VALUE;
		double max = 0;
		double sum = 0;

		for (int i = 0; i < transactions.size(); i++) {
			TransactionProcessed transaction = transactions.get(i);
			// remove invalid transactions
			if (!isValid(transaction, now)) {
				transactionsToDelete.add(transaction);
				continue;
			}
			count++;
			sum += transaction.getAmount();
			if (transaction.getAmount() < min) {
				min = transaction.getAmount();
			}
			if (transaction.getAmount() > max) {
				max = transaction.getAmount();
			}
		}
		double avg = count > 0 ? sum / count : 0;

		TransactionOutput output = new TransactionOutput();
		output.setAvg(avg);
		output.setCount(count);
		output.setMax(max);
		output.setMin(count > 0 ? min : 0);
		output.setSum(sum);

		statistics = output;

		// delete transactions
		transactions.removeAll(transactionsToDelete);
	}

	protected boolean isValid(TransactionProcessed transaction, Instant now) {
		return validator.isValid(transaction.getInstant(), now);
	}
}
