package br.com.lucasfelix.transactions.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class InstantValidator {
	private long validDifference;
	private ChronoUnit chronoUnit;

	public InstantValidator(long validDifference, ChronoUnit chronoUnit) {
		super();
		this.validDifference = validDifference;
		this.chronoUnit = chronoUnit;
	}

	public boolean isValid(Instant instant1, Instant instant2) {
		return chronoUnit.between(instant1, instant2) <= validDifference;
	}
}
