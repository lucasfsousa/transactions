package br.com.lucasfelix.transactions.rest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.lucasfelix.transactions.domain.TransactionInput;
import br.com.lucasfelix.transactions.domain.TransactionOutput;
import br.com.lucasfelix.transactions.service.TransactionService;

@RestController
@RequestMapping("/transactions")
public class TransactionsRest {
	private TransactionService service;

	public TransactionsRest(TransactionService service) {
		this.service = service;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> post(@RequestBody @NotNull @Valid TransactionInput input) {
		if (service.save(input)) {
			return new ResponseEntity<>(HttpStatus.CREATED);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(method = RequestMethod.GET)
	public TransactionOutput get() {
		return service.getStatistics();
	}
}
