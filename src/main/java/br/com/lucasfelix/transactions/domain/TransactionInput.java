package br.com.lucasfelix.transactions.domain;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class TransactionInput {
	@NotNull
	@Min(1)
	private Double amount;

	@NotNull
	private Long timestamp;

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
}
