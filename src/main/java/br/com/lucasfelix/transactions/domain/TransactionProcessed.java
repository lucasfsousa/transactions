package br.com.lucasfelix.transactions.domain;

import java.time.Instant;

public class TransactionProcessed {
	private Double amount;
	private Instant instant;

	public TransactionProcessed() {
		super();
	}

	public TransactionProcessed(Double amount, Instant instant) {
		super();
		this.amount = amount;
		this.instant = instant;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Instant getInstant() {
		return instant;
	}

	public void setInstant(Instant instant) {
		this.instant = instant;
	}

	@Override
	public String toString() {
		return "Amount: " + amount + ", instant:" + instant;
	}

}
