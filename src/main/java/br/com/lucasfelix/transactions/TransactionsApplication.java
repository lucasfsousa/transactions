package br.com.lucasfelix.transactions;

import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import br.com.lucasfelix.transactions.service.InstantValidator;
import br.com.lucasfelix.transactions.service.TransactionCollector;

@SpringBootApplication
@EnableScheduling
public class TransactionsApplication {
	private static final int VALID_DIFFERENCE_SECONDS = 60;

	public static void main(String[] args) {
		SpringApplication.run(TransactionsApplication.class, args);
	}

	@Bean
	public InstantValidator instantValidator() {
		return new InstantValidator(VALID_DIFFERENCE_SECONDS, ChronoUnit.SECONDS);
	}

	@Bean
	public TransactionCollector transactionProcessor(@Autowired InstantValidator validator) {
		return new TransactionCollector(validator);
	}
}
